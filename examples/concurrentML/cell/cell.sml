(* Implementing a shared memory cell which can be accessed concurrently  *)
structure Cell = struct
open CML
type 'a cell = { rChan : 'a chan, sChan : 'a chan }

fun make (x0 : 'a) : 'a cell
    = let val rc = channel ()
	  val sc = channel ()
	  fun serve x = let val xp = select [
				    wrap (recvEvt rc, fn xnew => xnew),
				    wrap (sendEvt (sc,x) , fn () => x)
				]
			in serve xp
			end

      in spawn (fn () => serve x0) ;
	 {rChan = rc, sChan = sc}
      end
fun get (c : 'a cell)          = recv (#sChan c)
fun put (c : 'a cell) (x : 'a)  = send (#rChan c, x)
end
