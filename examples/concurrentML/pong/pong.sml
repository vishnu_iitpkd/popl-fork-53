structure Pong = struct
open CML
val bound = 30

(* NOTE: avoid using print as it blocks. You should use the TextIO.print instead *)


(* Print a dying message *)
fun die prompt           = (TextIO.print (prompt ^ ": dying\n"); exit ())

(* Perform an action if the peer given by pid is not dead *)
fun alive prompt pid action = let val peerDead = wrap (joinEvt pid, fn () => die prompt)
			      in select [peerDead, action]
			      end
fun mainBody () =
    let val ch : int chan = channel ()
	fun ping x = let val _ = TextIO.print ("ping: sent " ^ Int.toString x ^ "\n")
			 val _ = send (ch, x)
			 val y = recv ch
		     in
		
			 if y > bound then die "ping" else ping y
		     end
			 
	val pingPid = spawn (fn () => ping 0)

	fun pong () = let val x = alive "pong" pingPid (recvEvt ch)
			  val _ = TextIO.print ("pong: received " ^ Int.toString x ^ "\n")
			  val _ = sync (timeOutEvt (Time.fromSeconds 1))
			  val _ = alive "pong" pingPid (sendEvt (ch, x + 1))
		      in
			  pong ()
		      end
		      
    

    in
	pong ()
    end
	
fun main ()  = RunCML.doit (mainBody, NONE)		     
end

