
// A trait is a property of a type. It captures what functions are available for the type.

pub trait Hello {
    fn hello (&self) -> String;
}

pub struct Name {
    pub first_name : String,
    pub last_name : String
}

impl Hello for Name {
    fn hello (&self) -> String
    {
	format!("Hello {}, {}", self.first_name, self.last_name)
    }
}

fn main()
{
    let jbond : Name = Name { first_name : String::from("James"),
			      last_name  : String::from("Bond")
    };

    println!("Hello message for: {}", jbond.hello() )

}
